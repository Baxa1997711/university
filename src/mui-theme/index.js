// https://mui.com/material-ui/customization/theming/

import { createTheme } from "@mui/material";
import { rem } from "utils/pxToRem";

export default createTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#fff",
      dark: "#333333",
      lightDark: "#1A1A1A",
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      light: "#0066ff",
      main: "#0044ff",
      // dark: will be calculated from palette.secondary.main,
      contrastText: "#ffcc00",
    },
  },
  typography: {
    fontFamily: "Gilroy",
    body1: {
      fontSize: rem(18),
      lineHeight: rem(24),
    },
    body2: {
      fontSize: rem(18),
      lineHeight: rem(28),
    },

    body3: {
      fontSize: rem(16),
      lineHeight: rem(24),
    },
    body4: {
      fontSize: rem(16),
      lineHeight: rem(18),
    },
    body5: {
      fontSize: rem(14),
      lineHeight: rem(16),
    },
    h1: {
      fontWeight: 700,
      fontSize: rem(56),
      lineHeight: rem(64),
    },
    h2: {
      fontWeight: 700,
      fontSize: rem(40),
      lineHeight: rem(50),
    },

    h3: {
      fontWeight: 600,
      fontSize: rem(32),
      lineHeight: rem(36),
    },

    h4: {
      fontSize: rem(20),
      loneHeight: rem(28),
    },

    h5: {
      fontWeight: 700,
      fontSize: rem(25),
      lineHeight: rem(37),
    },
    h6: {
      fontWeight: 700,
      fontSize: rem(20),
      lineHeight: rem(24),
    },
  },
  components: {
    MuiContainer: {
      styleOverrides: {
        root: {
          maxWidth: "1250px !important",
          // padding: "0 96px !important",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          backgroundColor: "red",
          "&:hover": {
            color: "black",
          },
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          scroller: {
            width: "100% !important",
          },
        },
      },
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          maxWidth: "386px !important",
        },
      },
    },
  },
});
