import React from "react";
import { Checkbox, Container, Typography } from "@mui/material";
import cls from "./AppSubmit.module.scss";
import FTextfield from "../FormElement/Input/FTextfield";
import CustomButton from "../Button/CustomButton";
import { rem } from "utils/pxToRem";

const AppSubmit = () => {
  return (
    <div className={cls.application}>
      <div className={cls.container}>
        <div className={cls.leftSide}>
          {/* <div className={cls.image}> */}
          <img src="/images/submitImage/photo.png" alt="photo" />
          {/* </div> */}
          {/* <div className={cls.image}> */}
          {/* <img src="/images/submitImage/secondImage.png" alt="photo" /> */}
          {/* </div> */}
          {/* <div className={cls.image}> */}
          {/* <img src="/images/submitImage/thirdImage.png" alt="photo" /> */}
          {/* </div> */}
        </div>
        <div className={cls.rightSide}>
          <Typography variant="h2" color="#1A2024">
            Qabul uchun ariza jo’nating
          </Typography>
          <form className={cls.form}>
            <FTextfield
              label="Telefon raqam"
              fullWidth
              placeHolder="Telefon raqamin kiriting"
            />
            <CustomButton fullWidth style={{ marginTop: rem(24) }}>
              Ariza qoldirish
            </CustomButton>
            <div className={cls.checkbox}>
              <Checkbox />
              <Typography
                variant="body5"
                lineHeight={rem(20)}
                fontWeight={rem(500)}
              >
                Shaxsiy ma'lumotlarni qayta ishlashga, pochta xabarlarini qabul
                qilishga, shuningdek, maxfiylik siyosatiga roziman
              </Typography>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AppSubmit;
