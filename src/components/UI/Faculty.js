import React, { useState } from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import { Container } from "@mui/material";
const FacultyTabs = () => {
  const [value, setValue] = useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Container>
        <Tabs
          onChange={handleChange}
          value={value}
          aria-label="Tabs where each tab needs to be selected manually"
        >
          <Tab label="Ijtimoiy fanlar" />
          <Tab label="Biznes va  boshqaruv" />
          <Tab label="Axborot-kommunikatsiya texnologiyalari" />
        </Tabs>
      </Container>
    </Box>
  );
};

export default FacultyTabs;
