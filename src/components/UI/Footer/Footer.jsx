import { Typography } from "@mui/material";
import Link from "next/link";
import { rem } from "utils/pxToRem";
import styles from "./Footer.module.scss";

export function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <div className={styles.box}>
          <Link href="/">
            <a className={styles.logo}>
              <img src="/images/footerImage/footerLogo.png" alt="logo" />
            </a>
          </Link>
          <div className={styles.network}>
            <Typography variant="body5" lineHeight={rem(20)} fontWeight="500">
              Bizni ijtimoiy tarmoqlar orqali kuzatishingiz mumkin.
            </Typography>
            <ul className={styles.list}>
              <li className={styles.listItem}>
                <Link href="#">
                  <img src="/images/footerImage/facebook.svg" alt="icon" />
                </Link>
              </li>
              <li className={styles.listItem}>
                <Link href="#">
                  <img src="/images/footerImage/telegram.svg" alt="icon" />
                </Link>
              </li>
              <li className={styles.listItem}>
                <Link href="#">
                  <img src="/images/footerImage/instagram.svg" alt="icon" />
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className={styles.facultyList}>
          <ul className={styles.list}>
            <li className={styles.listItem}>
              <Link href="#">
                <Typography variant="body4" fontWeight="500" component="a">
                  Fakultetlar
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <Link href="#">
                <Typography variant="body4" fontWeight="500" component="a">
                  Davlat litsenziyasi
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <Link href="#">
                <Typography variant="body4" fontWeight="500" component="a">
                  Qabul shartlari
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <Link href="#">
                <Typography variant="body4" fontWeight="500" component="a">
                  Dunyo bo'ylab
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <Link href="#">
                <Typography variant="body4" fontWeight="500" component="a">
                  Savol-javoblar
                </Typography>
              </Link>
            </li>
          </ul>
        </div>
        <div className={styles.workingList}>
          <ul className={styles.list}>
            <li className={styles.listItem}>
              <Typography variant="body4" fontWeight="600" lineHeight={rem(18)}>
                Ish vaqti
              </Typography>
            </li>
            <li className={styles.listItem}>
              <Typography variant="body4" fontWright="500">
                Dush-Juma
              </Typography>
              <Typography>09:00 - 20:00</Typography>
            </li>
            <li className={styles.listItem}>
              <Typography variant="body4" fontWright="500">
                Shanba
              </Typography>
              <Typography>09:00 - 19:00</Typography>
            </li>
          </ul>
        </div>
        <div className={styles.contactList}>
          <ul className={styles.list}>
            <li className={styles.listItem}>
              <Typography variant="body4" fontWeight="600" lineHeight={rem(18)}>
                Aloqa:
              </Typography>
            </li>
            <li className={styles.listItem}>
              <img src="/images/footerImage/message.png" alt="image" />
              <Link href="#">
                <Typography
                  variant="body3"
                  fontWeight="500"
                  marginLeft={rem(20)}
                  component="a"
                >
                  Telefon: +998 (93) 231-03-03
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <img src="/images/footerImage/phone.png" alt="image" />
              <Link href="#">
                <Typography
                  variant="body3"
                  fontWeight="500"
                  marginLeft={rem(20)}
                  component="a"
                >
                  E-mail: info@TOU.uz
                </Typography>
              </Link>
            </li>
            <li className={styles.listItem}>
              <img src="/images/footerImage/address.png" alt="image" />
              <Link href="#">
                <Typography
                  variant="body3"
                  fontWeight="500"
                  marginLeft={rem(20)}
                  component="a"
                >
                  Toshkent, Chilonzor tumani, Gavxar ko'chasi, 1-uy
                </Typography>
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <hr></hr>
      <div className={styles.bottomText}>
        <Typography variant="body4" fontWeight="400">
          ©TOU University 2022.Barcha huquqlar himoyalangan
        </Typography>
      </div>
    </footer>
  );
}
