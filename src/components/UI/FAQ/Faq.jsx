import { Typography } from "@mui/material";
import React, { useState } from "react";
import { rem } from "utils/pxToRem";
import cls from "./Faq.module.scss";
const Faq = () => {
  const [isClicked, setIsClicked] = useState(false);
  const data = [
    {
      title: "Universitetingizda byudjetli joylar bormi?",
      text: "Barcha universitetlar byudjet joylarini sezilarli darajada qisqartirgan, ammo bizning universitetimizda talabalar o'qishni ish bilan birlashtirish va o'qish uchun mustaqil ravishda to'lash imkoniyatiga ega bo'lgan maqsadli dasturlar mavjud.",
    },
    {
      title: "Kirish imtihonlari qanday o'tmoqda?",
      text: "Barcha universitetlar byudjet joylarini sezilarli darajada qisqartirgan, ammo bizning universitetimizda talabalar o'qishni ish bilan birlashtirish va o'qish uchun mustaqil ravishda to'lash imkoniyatiga ega bo'lgan maqsadli dasturlar mavjud.",
    },
    {
      title: "Men chegirma talab qila olamanmi?",
      text: "Barcha universitetlar byudjet joylarini sezilarli darajada qisqartirgan, ammo bizning universitetimizda talabalar o'qishni ish bilan birlashtirish va o'qish uchun mustaqil ravishda to'lash imkoniyatiga ega bo'lgan maqsadli dasturlar mavjud.",
    },
    {
      title: "Men chegirma talab qila olamanmi?",
      text: "Barcha universitetlar byudjet joylarini sezilarli darajada qisqartirgan, ammo bizning universitetimizda talabalar o'qishni ish bilan birlashtirish va o'qish uchun mustaqil ravishda to'lash imkoniyatiga ega bo'lgan maqsadli dasturlar mavjud.",
    },
  ];

  const toggle = (index) => {
    if (isClicked === index) {
      return setIsClicked(null);
    }
    setIsClicked(index);
  };

  return (
    <div className={cls.accordion}>
      <div className={cls.container}>
        <Typography variant="h2" color="primary.lightDark">
          Ko'p beriladigan savollar
        </Typography>
        <div className={cls.accordionList}>
          {data.map((item, index) => (
            <div
              onClick={() => toggle(index)}
              className={cls.accordionItem}
              key={index}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography variant="h6" color="primary.lightDark">
                  {item.title}
                </Typography>
                <span>
                  {isClicked === index ? (
                    <img
                      src="/images/accordionImage/close.png"
                      style={{ width: rem(40), height: rem(40) }}
                      alt="open"
                    />
                  ) : (
                    <img
                      src="/images/accordionImage/open.png"
                      style={{ width: rem(40), height: rem(40) }}
                      alt="close"
                    />
                  )}
                </span>
              </div>
              <div className={cls.text}>
                {isClicked === index ? (
                  <Typography
                    variant="body2"
                    color="primary.dark"
                    aria-expanded={!isClicked}
                  >
                    {item.text}
                  </Typography>
                ) : null}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Faq;
