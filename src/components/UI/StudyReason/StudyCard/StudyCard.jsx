import React from "react";
import { Container, Typography } from "@mui/material";
import cls from "./StudyCard.module.scss";
import { rem } from "utils/pxToRem";

const StudyCard = ({ icon, number, title, content }) => {
  return (
    <div className={cls.studyCard}>
      <div className={cls.container}>
        <div className={cls.header}>
          <img src={icon} className={cls.icon} alt="logo" />
          <p>{number}</p>
        </div>
        <div className={cls.box}>
          <Typography
            variant="h6"
            color="primary.lightDark"
            marginBottom={rem(8)}
          >
            {title}
          </Typography>
          <Typography variant="body3" color="primary.dark">
            {content}
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default StudyCard;
