import React, { useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Typography } from "@mui/material";
import StudyCard from "./StudyCard/StudyCard";
import cls from "./StudyReason.module.scss";
import { rem } from "utils/pxToRem";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import StudyCardBold from "./StudyCardBold/StudyCardBold";

const StudyReason = () => {
  const slider = useRef();
  const [isActive, setIsActive] = useState(0);

  const next = () => {
    setIsActive(isActive);
    slider.current.slickNext();
  };
  const previous = () => {
    console.log(isActive);
    slider.current.slickPrev();
  };
  const cards = [
    {
      icon: "/images/reasonImage/edu.png",
      title: "Zamonaviy dasturlar",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/teach.png",
      leftImage: "/images/reasonImage/vector.png",
      title: "Oliy toifali mutaxassislar",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/tick.png",
      title: "Yuqori sifat",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/lessons.png",
      title: "Amaliy darslar",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/edu.png",
      title: "Zamonaviy dasturlar",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/tick.png",
      title: "Yuqori sifat",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
    {
      icon: "/images/reasonImage/lessons.png",
      title: "Amaliy darslar",
      content: "biz o'rganish va o'sishni rag'batlantiramiz.",
    },
  ];

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    // autoplay: true,
    pauseOnHover: true,
    speed: 2000,
    nextArrow: (
      <NavigateNextIcon
        sx={{
          display: "none",
        }}
      />
    ),
    prevArrow: (
      <NavigateBeforeIcon
        sx={{
          display: "none",
        }}
      />
    ),
    // appendDots: (dots) => (
    //   <div>
    //     <ul style={{ margin: "0px" }}> {dots} </ul>
    //   </div>
    // ),
    // customPaging: (i) => (
    //   <div
    //     style={{
    //       width: rem(6),
    //       height: rem(6),
    //       color: "blue",
    //       background: isActive === i ? "#274441" : "#D9D9D9",
    //       borderRadius: rem(100),
    //       marginTop: rem(33),
    //       marginLeft: rem(-20),
    //     }}
    //   >
    //     {/* {isActive + i} */}
    //   </div>
    // ),
  };

  return (
    <div className={cls.study}>
      <div className={cls.container}>
        <div className={cls.box}>
          <Typography variant="h2">TOU da o'qish uchun 10 ta sabab</Typography>
          <div className={cls.buttons}>
            <button className={cls.prev}>
              <NavigateBeforeIcon
                onClick={previous}
                sx={{
                  color: "#84919A!important",
                  "&:hover": { color: "#333333!important" },
                }}
              />
            </button>
            <button className={cls.next}>
              <NavigateNextIcon
                onClick={next}
                sx={{
                  color: "#84919A!important",
                  "&:hover": { color: "#333333!important" },
                }}
              />
            </button>
          </div>
        </div>

        <div>
          <Slider ref={(c) => (slider.current = c)} {...settings}>
            {cards.map((item, index) => (
              <div key={index}>
                {item.title === "Oliy toifali mutaxassislar" ? (
                  <StudyCardBold
                    icon={item.icon}
                    leftImage={item.leftImage}
                    title={item.title}
                    content={item.content}
                  />
                ) : (
                  <StudyCard
                    icon={item.icon}
                    title={item.title}
                    content={item.content}
                  />
                )}
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default StudyReason;
