import { Typography } from "@mui/material";
import React from "react";
import { rem } from "utils/pxToRem";
import cls from "./StudyCardBold.module.scss";

const StudyCardBold = ({ icon, leftImage, number, title, content }) => {
  return (
    <div className={cls.studyCard}>
      <div className={cls.container}>
        <div className={cls.header}>
          <div className={cls.image}>
            <img src={icon} className={cls.icon} alt="logo" />
            <img src={leftImage} className={cls.leftImage} alt="img" />
          </div>
          <p>{number}</p>
        </div>
        <div className={cls.box}>
          <Typography variant="h6" color=" #FFFFFF" marginTop={rem(24)}>
            {title}
          </Typography>
          <Typography variant="body3" color="#FFFFFF" opacity="0.8">
            {content}
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default StudyCardBold;
