import React from "react";
import { Container, Typography } from "@mui/material";
import cls from "./LicenceDoc.module.scss";
import { rem } from "utils/pxToRem";

const LicenceDoc = () => {
  return (
    <div className={cls.licence}>
      {/* <Container className={cls.container}> */}
      <Typography className={cls.title} variant="h2">
        Davlat litsenziyasi
      </Typography>
      <div className={cls.box}>
        <div className={cls.leftSide}>
          <div className={cls.content}>
            <div style={{ marginTop: "12px" }}>
              <Typography
                className={cls.title1}
                variant="body4"
                fontWeight="700"
              >
                Asos:
              </Typography>
              <Typography variant="body2">
                O'zbekiston Respublikasi "Talim tugmasida" gi gonunini
                31-moddasiga asosan, Nodavlat ta’lim tashkilotlari o’z
                faoliyatini litsenziya asosida amalga oshiradi.
              </Typography>
            </div>
            <div style={{ marginTop: "8px" }}>
              <Typography
                className={cls.title2}
                variant="body4"
                fontWeight="700"
                marginTop={rem(8)}
              >
                Litsenziya:
              </Typography>
              <Typography className={cls.link} variant="body2">
                Universitet O'sbekiston Respubilikasi Vazirlar mahkamasi
                huzuridagi Ta’lim sifatini O'zbekiston Respublikasi "Talim
                to'grisida" gi qonunini 3-moddasiga asosan, Litsenziya davlat
                ta'lim standartiari.
                <a href="#">Batafsil</a>
              </Typography>
            </div>
          </div>
        </div>
        <div className={cls.rightSide}>
          <div className={cls.imageContent}>
            <Typography className={cls.leftContent} variant="h4">
              Ro'yxatdan o'tganlik to'g'risida guvohnoma
            </Typography>
          </div>
          <div className={cls.image}>
            <img src="/images/licenceImage/image.png" alt="image" />
          </div>
        </div>
      </div>
      {/* </Container> */}
    </div>
  );
};

export default LicenceDoc;
