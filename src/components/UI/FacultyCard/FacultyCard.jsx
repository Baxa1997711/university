import React from "react";
import { Typography } from "@mui/material";
import cls from "./FacultyCard.module.scss";
import { Box } from "@mui/system";

const FacultyCard = ({ img, background }) => {
  const cardinfo1 = [
    {
      title: "Qabul",
      text: "Obtyabr",
      icon: "./images/facultyImage/admissionIcon.svg",
    },
    {
      title: "Ta'lim shakli",
      text: "Sirtqi",
      icon: "./images/facultyImage/eduIcon.svg",
    },
    {
      title: "Manzil",
      text: "Online",
      icon: "./images/facultyImage/locationIcon.svg",
    },
    {
      title: "Muddati",
      text: "4yil",
      icon: "./images/facultyImage/deadlineIcon.svg",
    },
  ];

  const cardinfo2 = [
    {
      title: "Narxi",
      text: "10 million",
      icon: "./images/facultyImage/priceIcon.svg",
    },

    {
      title: "Diplom",
      text: "Davlat tomonidan tasdiqlangan namunadagi diplom",
      icon: "./images/facultyImage/diplomIcon.svg",
    },
    {
      title: "Kirish imtixoni",
      text: "120 ta intellektual qobilyat testlar yoki joriy yil DTM varaqasi  (56.7ball)",
      icon: "./images/facultyImage/examIcon.svg",
    },
    {
      title: "Hujjatlar",
      text: "Passport/Diplom yoki 11-sinf shahodatnomasi",
      icon: "./images/facultyImage/docsIcon.svg",
    },
  ];

  return (
    <div
      className={cls.faculty}
      style={{
        background: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "100% 100%",
      }}
    >
      <div className={cls.root}>
        <div className={cls.leftSide}>
          <Typography className={cls.title} variant={"h3"}>
            Jurnalistika
          </Typography>
          <Typography className={cls.content} variant={"body2"}>
            Umumiy о‘rta ta’lim maktablari, kasb-hunar kollejlari uchun
            о‘qituvchi tayyorlash jarayonida foydalaniladigan fanlar, pedagogik
            faoliyat vositalari, usullari, metodlari yig‘indisidan iborat
            bо‘lgan ta’lim yо‘nalishidir.
          </Typography>
          <div className={cls.listIcon}>
            <ul className={cls.list1}>
              {cardinfo1.map((item, index) => (
                <li className={cls.listItem} key={index}>
                  <img src={item.icon} alt="icon" />
                  <div className={cls.block}>
                    <Typography className={cls.listTitle} variant={"body5"}>
                      {item.title}
                    </Typography>{" "}
                    <br></br>
                    <Typography className={cls.listText} variant={"body4"}>
                      {item.text}
                    </Typography>
                  </div>
                </li>
              ))}
            </ul>
            <ul className={cls.list2}>
              {cardinfo2.map((item, index) => (
                <li className={cls.listItem} key={index}>
                  <img src={item.icon} alt="icon" />
                  <div className={cls.block}>
                    <Typography className={cls.listTitle} variant={"body4"}>
                      {item.title}
                    </Typography>{" "}
                    <br></br>
                    <Typography className={cls.listText} variant={"body4"}>
                      {item.text}
                    </Typography>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className={cls.rightSide}>
          <img src={img} alt="pic" />
        </div>
      </div>
    </div>
  );
};

export default FacultyCard;
