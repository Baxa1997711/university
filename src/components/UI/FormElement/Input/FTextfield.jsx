import React from "react";
import cls from "./CustomInput.module.scss";

const FTextfield = ({
  label,
  type = "",
  size = "small",
  className = "",
  fullWidth,
  placeHolder,
  ...restProps
}) => {
  return (
    <div className={cls.inputLabel}>
      <label className={cls.label}>{label}</label>
      <br></br>
      <input
        type={type}
        className={`${cls.input} ${className} ${
          fullWidth ? cls.fullWidth : ""
        }`}
        placeholder={placeHolder}
        {...restProps}
      />
    </div>
  );
};

export default FTextfield;
