import React from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { Controller } from "react-hook-form";

const FSelect = ({
  label,
  options = [],
  placeholder,
  required,
  ...restProps
}) => {
  return (
    <Controller>
      <FormControl>
        <InputLabel>{label}</InputLabel>
        <Select
          value={value || ""}
          size="small"
          fullWidth
          inputProps={{ placeholder }}
          onChange={() => onChange(e.target.value)}
          {...restProps}
        >
          {options?.map((item) => (
            <MenuItem key={option.value} value={option.value}>
              {item.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Controller>
  );
};

export default FSelect;
