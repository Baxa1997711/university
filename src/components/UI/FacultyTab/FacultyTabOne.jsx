import React from "react";
import FacultyCard from "../FacultyCard/FacultyCard";

const FacultyTabOne = () => {
  return (
    <div>
      <FacultyCard
        img={"./images/facultyImage/cardImage1.png"}
        background={"./images/facultyImage/backgroundImage1.png"}
      />
      <FacultyCard
        img={"./images/facultyImage/cardImage2.png"}
        background={"./images/facultyImage/backgroundImage2.png"}
      />
      <FacultyCard
        img={"./images/facultyImage/cardImage3.png"}
        background={"./images/facultyImage/backgroundImage3.png"}
      />
    </div>
  );
};

export default FacultyTabOne;
