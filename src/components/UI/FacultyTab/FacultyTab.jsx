import React, { useState } from "react";
import FacultyCard from "components/UI/FacultyCard/FacultyCard";
import cls from "./FacultyTab.module.scss";
import { Container, Typography } from "@mui/material";
import { rem } from "utils/pxToRem";
import { Box } from "@mui/system";
import FacultyTabOne from "./FacultyTabOne";
import FacultyTabTwo from "./FacultyTabTwo";
import FacultyTabThree from "./FacultyTabThree";

const FacultyTab = () => {
  const [activeTab, setActiveTab] = useState("tab1");

  const handleTab = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className={cls.tabs}>
      <div className={cls.container}>
        <Typography variant={"h2"}>Fakultetlar</Typography>
        <ul className={cls.tabList}>
          <li
            className={` ${cls.tabItem} ${
              activeTab === "tab1" ? cls.activeTab : ""
            }`}
            onClick={() => handleTab("tab1")}
          >
            <Typography variant="h6" lineHeight={rem(24)} fontWeight="700">
              Ijtimoiy fanlar
            </Typography>
          </li>
          <li
            className={` ${cls.tabItem} ${
              activeTab === "tab2" ? cls.activeTab : ""
            }`}
            onClick={() => handleTab("tab2")}
          >
            <Typography variant="h6" lineHeight={rem(24)} fontWeight="700">
              Biznes va boshqaruv
            </Typography>
          </li>
          <li
            className={` ${cls.tabItem} ${
              activeTab === "tab3" ? cls.activeTab : ""
            }`}
            onClick={() => handleTab("tab3")}
          >
            <Typography variant="h6" lineHeight={rem(24)} fontWeight="700">
              Axborot-kommunikatsiya texnologiyalari
            </Typography>
          </li>
        </ul>

        <Box className={cls.tabContent}>
          <div
            className={` ${cls.panelContent} ${
              activeTab === "tab1" ? cls.activeContent : ""
            }`}
          >
            <FacultyTabOne />
          </div>
          <div
            className={` ${cls.panelContent} ${
              activeTab === "tab2" ? cls.activeContent : ""
            }`}
          >
            <FacultyTabTwo />
          </div>
          <div
            className={` ${cls.panelContent} ${
              activeTab === "tab3" ? cls.activeContent : ""
            }`}
          >
            <FacultyTabThree />
          </div>
        </Box>
      </div>
    </div>
  );
};

export default FacultyTab;
