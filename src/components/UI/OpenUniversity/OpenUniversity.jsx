import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Container, Typography } from "@mui/material";
import cls from "./OpenUniversity.module.scss";
import { rem } from "utils/pxToRem";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";

const OpenUniversity = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 8,
    slidesToScroll: 5,
    // autoplay: true,
    speed: 2000,
    pauseOnHover: true,
    nextArrow: (
      <NavigateNextIcon
        sx={{
          color: "#84919A!important",
          "&:hover": { color: "#333333!important" },
          marginTop: "-30px",
        }}
      />
    ),
    prevArrow: (
      <NavigateBeforeIcon
        sx={{
          color: "#84919A!important",
          "&:hover": { color: "#333333!important" },
          marginTop: "-30px",
        }}
      />
    ),
  };

  const logos = [
    "/images/universityLogo/logo1.png",
    "/images/universityLogo/logo2.png",
    "/images/universityLogo/logo3.png",
    "/images/universityLogo/logo4.png",
    "/images/universityLogo/logo5.png",
    "/images/universityLogo/logo6.png",
    "/images/universityLogo/logo7.png",
    "/images/universityLogo/logo8.png",
    "/images/universityLogo/logo1.png",
  ];
  return (
    <div className={cls.university}>
      <div className={cls.container}>
        <div className={cls.title}>
          <Typography variant="h2" color="primary.dark">
            Open Universiteti dunyo bo’ylab
          </Typography>
        </div>
        <div className={cls.content}>
          <Typography variant="body2" color="primary.dark">
            Ochiq universitetlar - kirish talablari oson yoki umuman talab
            qilinmaydigan darajalarni taklif qiluvchi oli ta'lim
            muassasalaridir. Ularni alohida ajratib turadigan yana bir jihati
            shundaki, ular masofaviy ta'lim texnologiyalarini keng qo'llaydi,
            ya'ni o'qish uchun universitetga borish shart emas. Shunday qilib,
            kurslar to'liq onlayn yoki uyingizga yuborilgan materiallar bilan
            o'qitiladi.
          </Typography>
          <Typography className={cls.link} variant="body2" color="primary.dark">
            Ochiq universitetlarning maqsadi har kimga o'z qobiliyatlarini
            rivojlantirish, ta'lim darajasini oshirish va yangi martaba uchun
            qayta tayyorlash uchun keng imkoniyatlarni taqdim etishdir.{" "}
            <a href="#">Batafsil</a>
          </Typography>
        </div>
        <div className={cls.logoSlider}>
          <Slider {...settings}>
            {logos.map((item) => (
              <img
                key={item}
                src={item}
                alt="logo"
                wdith="120px!important"
                style={{
                  width: rem(120),
                  height: rem(120),
                }}
                margin={rem(0)}
              />
            ))}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default OpenUniversity;
