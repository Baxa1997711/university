import Link from "next/link";
import { useState } from "react";
import useTranslation from "next-translate/useTranslation";
import { Container, Typography } from "@mui/material";
import styles from "./Header.module.scss";
import CustomButton from "components/UI/Button/CustomButton";
import { rem } from "utils/pxToRem";

export function Header() {
  const { t } = useTranslation("common");
  const [styleList, setStyleList] = useState();

  const navbarList = [
    {
      title: t("faculty"),
      link: "#",
    },
    {
      title: t("licence"),
      link: "#",
    },
    {
      title: t("admission"),
      link: "#",
    },
    {
      title: t("around_world"),
      link: "#",
    },
    {
      title: t("take_contract"),
      link: "#",
    },

    {
      title: t("question"),
      link: "#",
    },
  ];

  return (
    <header className={styles.header}>
      <Container>
        <div className={styles.box}>
          <div className={styles.rightElement}>
            <Link href="/">
              <a className={styles.logo}>
                <img src="./images/logo.svg" alt="logo" />
              </a>
            </Link>
            <nav>
              <ul>
                {navbarList.map((el, index) => (
                  <li
                    key={index}
                    className={
                      styleList === index ? styles.linkRoute : styles.link
                    }
                    onClick={() => setStyleList(index)}
                  >
                    <Link href={el.link}>
                      <Typography
                        variant="body3"
                        lineHeight={rem(18)}
                        fontWeight="500"
                        component="a"
                        color="primary.dark"
                      >
                        {el.title}
                      </Typography>
                    </Link>
                  </li>
                ))}
              </ul>
            </nav>
          </div>
          <CustomButton size="small">Ariza qoldirish</CustomButton>
        </div>
      </Container>
    </header>
  );
}
