import React from "react";
import { Container, Typography } from "@mui/material";
import cls from "./AdmissionCard.module.scss";
import { rem } from "utils/pxToRem";

const AdmissionCard = ({ icon, number, title, content }) => {
  return (
    <div className={cls.card}>
      <Container className={cls.container}>
        <div className={cls.header}>
          <img src={icon} className={cls.icon} alt="logo" />
          <p>{number}</p>
          {/* <img src={image} className={cls.image} alt="logo" /> */}
        </div>
        <div className={cls.box}>
          <Typography
            variant="h6"
            color="primary.lightDark"
            marginBottom={rem(8)}
          >
            {title}
          </Typography>
          <Typography
            variant="body3"
            color="primary.dark"
            marginBottom={rem(24)}
          >
            {content}
          </Typography>
        </div>
      </Container>
    </div>
  );
};

export default AdmissionCard;
