import React from "react";
import { Container, Grid, Typography } from "@mui/material";
import AdmissionCard from "./AdmissionCard/AdmissionCard";
import cls from "./Admission.module.scss";
import { rem } from "utils/pxToRem";

const Admission = () => {
  const card = [
    {
      icon: "/images/admissionImage/questionIcon.png",
      number: "1",
      // image: "/images/admissionImage/1.png",
      title: "So'rov qoldiring",
      content: "Mobil raqamingizdan foydalanib onlayn ariza bering.",
    },
    {
      icon: "/images/admissionImage/docIcon.png",
      number: "2",

      image: "/images/admissionImage/2.png",
      title: "Hujjatlarni taqdim eting",
      content:
        "Tizimga kerakli hujjatlarni, shu jumladan, joriy ta'lim darajangiz to'g'risidagi hujjatlarni ham taqdim eting.",
    },
    {
      icon: "/images/admissionImage/testIcon.png",
      number: "3",

      image: "/images/admissionImage/3.png",
      title: "Imtihonlar",
      content:
        "Universitetning ichki kirish imtihonlarini topshirish yoki kirish imtihonlari natijalari to'g'risida ma'lumot berish.",
    },
    {
      icon: "/images/admissionImage/contractIcon.png",
      number: "4",

      image: "/images/admissionImage/4.png",
      title: "Shartnoma (Kontrakt)",
      content:
        "Javoblar e'lon qilinganidan so'ng, shartnomani rasmiylashtiring.",
    },
  ];

  return (
    <div className={cls.admission}>
      <div className={cls.container}>
        <Typography variant="h2" color="primary.lightDark">
          Qabul shartlari
        </Typography>
        <Grid container>
          {card.map((item, index) => (
            <Grid xs={6} md={3} key={index}>
              <AdmissionCard
                icon={item.icon}
                number={item.number}
                title={item.title}
                content={item.content}
              />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default Admission;
