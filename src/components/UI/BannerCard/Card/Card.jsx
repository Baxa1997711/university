import React from "react";
import { Container, Typography } from "@mui/material";
import CustomButton from "../../Button/CustomButton";
import cls from "./Card.module.scss";

const Card = () => {
  return (
    <div className={cls.card}>
      <Container className={cls.container}>
        <Typography className={cls.title} variant="h2">
          Hali ham yo‘nalish tanlay olmadingizmi?
        </Typography>
        <Typography
          className={cls.content}
          variant="body2"
          color="primary.dark"
        >
          Unda biz sizga yordam beramiz! Biz o’z sohasiga ishtiyoqi va
          muaffaqiyatga intilishi kuchli bo’lgan ambitsiyali talabalarini
          qidiramiz.
        </Typography>
        <div className={cls.buttons}>
          <CustomButton type="button" size="small">
            Boshlash
          </CustomButton>
          <CustomButton size="small" className={cls.questionBtn}>
            Savollaringiz
          </CustomButton>
        </div>
      </Container>
    </div>
  );
};

export default Card;
