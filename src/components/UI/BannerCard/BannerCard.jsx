import React from "react";
import Card from "./Card/Card";
import cls from "./BannerCard.module.scss";
import { Box } from "@mui/system";

const BannerCard = () => {
  return (
    <div className={cls.card}>
      <Box className={cls.box}>
        <Card />
      </Box>
    </div>
  );
};

export default BannerCard;
