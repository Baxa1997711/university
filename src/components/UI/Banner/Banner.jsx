import React, { useState, useEffect } from "react";
import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import { Button, Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from "./Banner.module.scss";
import CustomButton from "components/UI/Button/CustomButton";

export const Banner = () => {
  const [progress, setProgress] = useState(0);
  const [activeItem, setActiveItem] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= 100 ? 0 : prevProgress + 5
      );
    }, 250);

    return () => {
      clearInterval(timer);
    };
  }, []);

  const settings = {
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 4000,
    variableWidth: true,
    // autoplaySpeed: progress,
    cssEase: "linear",
    fade: true,
  };

  const images = [
    "./images/bannerImage/bannerImage-1.png",
    "https://img.freepik.com/free-photo/wide-angle-shot-single-tree-growing-clouded-sky-during-sunset-surrounded-by-grass_181624-22807.jpg?w=2000",
    "https://img.freepik.com/free-photo/breathtaking-shot-beautiful-stones-turquoise-water-lake-hills-background_181624-12847.jpg?w=2000",
  ];

  const img = [
    "./images/bannerImage/circular1.svg",
    "./images/bannerImage/circular2.svg",
    "./images/bannerImage/circular3.svg",
  ];

  return (
    <div className={styles.banner}>
      <Container className={styles.container}>
        <Box className={styles.box}>
          <div className={styles.row}>
            <div className={styles.leftSide}>
              <div className={styles.text}>
                <Typography variant={"h1"} className={styles.title}>
                  Kelajak pedagogi O‘qishga ariza qoldir!
                </Typography>
                <Typography variant={"body1"} className={styles.content}>
                  Biz kelajakka tayyor va sohaga tegishli kurslarni taqdim etish
                  orqali ertangi kunning yetakchilarini rivojlantiruvchi global
                  institutmiz.
                </Typography>
              </div>
              <div className={styles.btnGroup}>
                <CustomButton size={"medium"}>Ariza qoldirish</CustomButton>
              </div>
            </div>
            <div className={styles.rightSide}>
              <Slider {...settings} afterChange={(e) => setActiveItem(e)}>
                {images.map((url) => (
                  <img
                    key={url}
                    src={url}
                    alt="nature"
                    style={{
                      objectFit: "cover",
                      width: "415px !important",
                      height: "419px",
                    }}
                  />
                ))}
              </Slider>
              {/* <img
                src="./images/bannerImage/bannerImage-1.png"
                alt="banner-image"
              /> */}
            </div>
          </div>
        </Box>
        <Box className={styles.circularImage}>
          {img.map((item, index) => (
            <Stack
              key={item}
              spacing={2}
              direction="row"
              style={{ position: "relative", top: "-20px" }}
            >
              <span style={{ position: "absolute", top: 2, left: 18.2 }}>
                <img
                  src={item}
                  style={{
                    width: activeItem === index ? "45px" : "40px",
                    transition: activeItem === index ? ".5s" : "",
                  }}
                  alt="photo"
                />
              </span>
              <CircularProgress
                variant="determinate"
                style={{
                  color: activeItem === index ? "#274441" : "transparent",
                }}
                value={progress}
                text
                size={50}
              />
            </Stack>
          ))}
        </Box>
      </Container>
    </div>
  );
};
