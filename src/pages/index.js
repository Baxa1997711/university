import { Main } from "components/UI/Main/Main";
import SEO from "components/SEO";
import { fetchMultipleUrls } from "services/fetchMultipleUrls";
import FacultyTabs from "components/UI/Faculty";
import { Banner } from "components/UI/Banner/Banner";
import FacultyTab from "components/UI/FacultyTab/FacultyTab";
import BannerCard from "components/UI/BannerCard/BannerCard";
import LicenceDoc from "components/UI/LicenceDoc/LicenceDoc";
import Admission from "components/UI/Admission/Admission";
import OpenUniversity from "components/UI/OpenUniversity/OpenUniversity";
import AppSubmit from "components/UI/AppSubmit/AppSubmit";
import Faq from "components/UI/FAQ/Faq";
import StudyReason from "components/UI/StudyReason/StudyReason";

export default function Home({ data }) {
  return (
    <div style={{ background: "#f7f7f7" }}>
      <SEO />
      <Banner />
      <FacultyTab />
      <BannerCard />
      <StudyReason />
      <LicenceDoc />
      {/* <FacultyCard /> */}
      <Admission />
      <OpenUniversity />
      <AppSubmit />
      <Faq />
      {/* <Main /> */}
    </div>
  );
}

export async function getServerSideProps(context) {
  const urls = ["/posts"];
  const data = await fetchMultipleUrls(urls);

  return {
    props: {
      data,
    },
  };
}
